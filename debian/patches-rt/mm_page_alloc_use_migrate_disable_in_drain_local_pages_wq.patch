From: Sebastian Andrzej Siewior <bigeasy@linutronix.de>
Subject: mm: page_alloc: Use migrate_disable() in drain_local_pages_wq()
Date: Fri, 15 Oct 2021 23:09:33 +0200
Origin: https://www.kernel.org/pub/linux/kernel/projects/rt/5.15/older/patches-5.15.2-rt20.tar.xz

drain_local_pages_wq() disables preemption to avoid CPU migration during
CPU hotplug and can't use cpus_read_lock().

Using migrate_disable() works here, too. The scheduler won't take the
CPU offline until the task left the migrate-disable section.
The problem with disabled preemption here is that drain_local_pages()
acquires locks which are turned into sleeping locks on PREEMPT_RT and
can't be acquired with disabled preemption.

Use migrate_disable() in drain_local_pages_wq().

Signed-off-by: Sebastian Andrzej Siewior <bigeasy@linutronix.de>
Link: https://lore.kernel.org/r/20211015210933.viw6rjvo64qtqxn4@linutronix.de
---
 mm/page_alloc.c |    4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)
---
--- a/mm/page_alloc.c
+++ b/mm/page_alloc.c
@@ -3147,9 +3147,9 @@ static void drain_local_pages_wq(struct
 	 * cpu which is alright but we also have to make sure to not move to
 	 * a different one.
 	 */
-	preempt_disable();
+	migrate_disable();
 	drain_local_pages(drain->zone);
-	preempt_enable();
+	migrate_enable();
 }
 
 /*
